package com.techieteamltd.ecom.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.techieteamltd.ecom.Model.Product
import com.techieteamltd.ecom.Model.Productlist
import com.techieteamltd.ecom.R

class ProductlistAdapter(val productList : List<Product>) : RecyclerView.Adapter<ProductListViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.productlist_design,parent, false)
        return ProductListViewHolder(view)
    }


    override fun getItemCount(): Int {
        return productList.size

    }

    override fun onBindViewHolder(holder: ProductListViewHolder, position: Int) {
        return holder.bind(productList[position])
    }
}

class ProductListViewHolder(itemView : View): RecyclerView.ViewHolder(itemView) {
    private val photo: ImageView = itemView.findViewById(R.id.product_photo)
    private val product_name: TextView = itemView.findViewById(R.id.product_name)
    private val product_price: TextView = itemView.findViewById(R.id.product_price)
    private val rc: ImageView = itemView.findViewById(R.id.removeItem)
    private val ac: ImageView = itemView.findViewById(R.id.addToCart)


    fun bind(product: Product) {
        Glide.with(itemView.context).load(product.image).into(photo)
        product_name.text = product.name
        product_price.text = product.price

       rc.setOnClickListener(object : View.OnClickListener{
           override fun onClick(p0: View?) {
               System.out.println("item removed")
                         }
       })
        ac.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {
                System.out.println("item added to cart")
            }
        } )

    }

}




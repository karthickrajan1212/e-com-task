package com.techieteamltd.ecom.UIScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.techieteamltd.ecom.MainActivity
import com.techieteamltd.ecom.R

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler(Looper.getMainLooper()).postDelayed({
            gotologin()
    },3000)
}
    fun gotologin(){
        val intentObj = Intent(this,MainActivity::class.java)
        startActivity(intentObj)
        finish()
    }

}
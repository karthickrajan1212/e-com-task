package com.techieteamltd.ecom.UIScreen

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.techieteamltd.ecom.Adapter.ProductlistAdapter
import com.techieteamltd.ecom.ApiConfig.ApiEndpoints
import com.techieteamltd.ecom.ApiConfig.Retrofitconfig
import com.techieteamltd.ecom.Model.Productlist
import com.techieteamltd.ecom.R
import kotlinx.android.synthetic.main.activity_product_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)
        callapi()
    }
    fun callapi()
    {
        val request = Retrofitconfig.buildService(ApiEndpoints::class.java)
        val call = request.getProductlist()
        call.enqueue(object : Callback<Productlist> {
            override fun onResponse(call: Call<Productlist>, response: Response<Productlist>) {
                if (response.isSuccessful){
                    recyclerView.apply {
                        setHasFixedSize(true)
                        layoutManager = LinearLayoutManager(this@ProductListActivity)
                        adapter = ProductlistAdapter(response.body()!!.products)
                    }
                }
                progress_bar.visibility = View.GONE

            }

            override fun onFailure(call: Call<Productlist>, t: Throwable) {
                progress_bar.visibility = View.GONE
            }

        })
    }
}

package com.techieteamltd.ecom

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.techieteamltd.ecom.UIScreen.ProductListActivity

class MainActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var loginbutton: Button
    lateinit var username: EditText
    lateinit var password: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        username = findViewById(R.id.userid)
        password = findViewById(R.id.passwordid)
        loginbutton = findViewById(R.id.Loginid)
        loginbutton.setOnClickListener(this)
    }
    override fun onClick(v: View) {
        val usertext: String = username.getText().toString().trim()
        val pw: String = password.getText().toString()
        if (v == loginbutton) {
            if (usertext.isNotEmpty() && pw.isNotEmpty()) {
                if (isEmailValid(usertext)) {
                    System.out.println("input received")
                    if (pw.length > 6 && pw.length < 12) {
                        val io = Intent(this, ProductListActivity::class.java)
                        Toast.makeText(this, "login successfully", Toast.LENGTH_SHORT).show()
                        startActivity(io)
                        finish()
                    } else {
                        Toast.makeText(this, "must need 6 to 12 letters", Toast.LENGTH_SHORT).show()
                    }
                }else {
                        Toast.makeText(applicationContext, "Invalid email address", Toast.LENGTH_SHORT).show()
                    }

                } else {
                    System.out.println("no inputs provided")
                    Toast.makeText(this, "Please enter both fields", Toast.LENGTH_SHORT).show()
                }
            }
        }

    fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}



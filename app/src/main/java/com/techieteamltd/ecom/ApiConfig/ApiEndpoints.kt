package com.techieteamltd.ecom.ApiConfig

import com.techieteamltd.ecom.Model.Productlist
import retrofit2.Call
import retrofit2.http.GET

interface ApiEndpoints {
    @GET("v2/5def7b172f000063008e0aa2")
    fun getProductlist(): Call<Productlist>
}
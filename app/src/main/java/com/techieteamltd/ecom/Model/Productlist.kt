package com.techieteamltd.ecom.Model

data class Productlist(
    val products: List<Product>
)